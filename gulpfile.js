var gulp = require('gulp'),
		sass = require('gulp-sass'),
		server = require('gulp-webserver');

gulp.task('html', function(){
	gulp.src('app/views/**/*.html');
});
gulp.task('js', function(){
	gulp.src('app/js/**/*.js');
});
gulp.task('sass', function(){
	gulp
		.src('app/sass/**/*.scss')
		.pipe(sass())
		.pipe(gulp.dest('app/css/'));
});

gulp.task('watch', function(){
	gulp.watch('app/views/**/*html', ['html']);
	gulp.watch('app/js/**/*js', ['js']);
	gulp.watch('app/sass/**/*scss', ['sass']);
});

gulp.task('server', function(){
	gulp
		.src('./')
		.pipe(server({
			livereload: true,
			port: 8080
		}))
})

gulp.task('develop', [
	'html',
	'js',
	'sass',
	'server',
	'watch'
])