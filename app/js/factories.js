angular
	.module('factory', [])
	.factory('getUsers', ['$http', function($http){

		var users = {};
		users.usersInfo = function(){ 
			return $http.get('app/users.json');
		}
		return users;
	}]);