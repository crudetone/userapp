angular
	.module('router', ['ui.router'])
	.config(['$urlRouterProvider', '$stateProvider', '$locationProvider', function($urlRouterProvider, $stateProvider, $locationProvider){
		$urlRouterProvider.otherwise('/');
		$stateProvider
			.state('home', {
				url: '/',
				templateUrl: 'app/views/home.html',
				controller: 'userCtrl',
				controllerAs: 'vm',
			})
			.state('user-edit', {
				url: '/user/:id',
				templateUrl: 'app/views/user-edit.html',
				controller: 'userEditCtrl',
				controllerAs: 'vm',
			})
			.state('about', {
				url: '/about',
				templateUrl: 'app/views/about.html'
			})
			.state('contact', {
				url: '/contact',
				templateUrl: 'app/views/contact.html'
			});
	}]);