# README #

This README would normally document whatever steps are necessary to get your application up and running.

### How do I get set up? ###

### **git clone** ###
* git clone https://crudetone@bitbucket.org/crudetone/userapp.git
* cd userapp

### **install dependencies** ###
* npm install
* bower install

### **to start local server** ###
* gulp develop

### **go to link in your browser** ###
* [http://localhost:8080](Link URL)